<?php
/**
 * Created by PhpStorm.
 * 
 * User: sanderhagenaars
 * Date: 20/04/16
 * Time: 11:49
 *
 * @property SiteConfig|TrackingConfig $owner
 */

class TrackingConfig extends DataExtension {

	private static $ga_user_id;
	private static $ga_site_url;

	private static $fb_user_id;

	public static function current()
	{
		return SiteConfig::current_site_config();
	}

	public static function get_ga_user_id()
	{
		return self::config()->ga_user_id;
	}

	public static function get_ga_site_url()
	{
		return self::config()->ga_site_url;
	}

	public static function get_fb_user_id()
	{
		return self::config()->fb_user_id;
	}

	/**
	 * Helper for getting static shop config.
	 * The 'config' static function isn't avaialbe on Extensions.
	 *
	 * @return Config_ForClass configuration object
	 */
	public static function config()
	{
		return new Config_ForClass("TrackingConfig");
	}

	/**
	 * Facebook pixel ID for templates
	 * return string
	 */
	public function FacebookPixelID(){
		return self::config()->fb_user_id;
	}
	
} 
