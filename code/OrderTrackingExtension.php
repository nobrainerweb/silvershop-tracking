<?php
/**
 * Created by PhpStorm.
 * 
 * User: sanderhagenaars
 * Date: 20/04/16
 * Time: 11:34
 *
 * @property Order|OrderTrackingExtension $owner
 * @property boolean $AnalyticsTracked
 */

class OrderTrackingExtension extends DataExtension {

	private static $db = [
		'AnalyticsTracked' => 'Boolean(0)'
	];

	private static $defaults = [
		'AnalyticsTracked' => 0
	];

	public function onPaid(){
		if(!$this->isAnalyticsTracked()){
			$this->sendTrackingCodes();
			$this->updateTrackedStatus();
		}
	}

	public function sendTrackingCodes(){
		// total of order
		$order_total = $this->owner->Total();

		// send to google and facebook the order amount

		// Facebook conversion
		//TODO  No longer works. Is implemented in templates now
		//if($user_id = TrackingConfig::get_fb_user_id()){
			//$url = 'https://www.facebook.com/tr?id={{facebook pixel}}&ev=Purchase&cd[value]=10.00&cd[currency]=USD&cd[content_name]=Product%20Name&cd[content_type]=product&cd[content_ids]=woo-123&cd[num_items]=1&noscript=1';
			//$pageviewurl = 'https://www.facebook.com/tr?id=' . $user_id . '&ev=PageView';
			//$purchaseurl = 'https://www.facebook.com/tr?id=' . $user_id . '&ev=Purchase&cd[order_id]=' . $this->owner->ID . '&cd[value]=' . $order_total . '&cd[currency]=DKK&noscript=1';
			//$response = fopen($pageviewurl, "r");
			//$response = fopen($purchaseurl, "r");
			//$this->owner->createStatusLog('Order Update', 'The facebook tracking pixel on order returned the following: ' . implode('; ', $http_response_header));
		//}

		// Google
		/*
		if(class_exists('ssga') && TrackingConfig::get_ga_user_id()){
			$ssga = new ssga(TrackingConfig::get_ga_user_id(), TrackingConfig::get_ga_site_url());
			$ssga->set_event( 'FlammenHomeMade', 'OrdreBestilling', '', $order_total);
			$ssga->send();
		}*/
	}

	public function updateTrackedStatus(){
		$order = $this->owner;
		$order->AnalyticsTracked = 1;
		$order->write();
		//$this->owner->createStatusLog('Order Update', 'Facebook pixel tracked');
	}

	/**
	 * @return bool
	 */
	public function isAnalyticsTracked(){
		return $this->owner->AnalyticsTracked;
	}
}
